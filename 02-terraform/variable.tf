variable "name" {
  default = "jihu-zhaopeng"
}

variable "region" {
  default = "ap-chongqing-1"
}

variable "k8s_ver" {
  default = "1.20.4"
}

variable "pod_ip_seg" {
  default = "172.16"
}

variable "vpc_ip_seg" {
  default = "10.0"
}

variable "default_instance_type" {
  default = "S2.MEDIUM4"
}

variable "node_password" {
  default = "123456qwer"
}
